import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

    const data = {
        title: "UpCARming...",
        content: "Get upcoming car of the year before everyone does",
        destination: "/products",
        label: "Reserve now!"
    }
    return (
        <div className="home page-container">
			<Fragment>
				<Banner data={data}/>
				<Highlights/>
			</Fragment>
		</div>
    )
};