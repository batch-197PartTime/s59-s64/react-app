import { Fragment, useState, useContext } from 'react';
// import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Orders() {

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	// useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setOrders(data.map(product => {
				return (
					<ProductCard  key={product._id} productProp={product} />
				)
			}))

		})
	// }, [])	

	return (
		(user.isAdmin)
		?
			<Navigate to="/orders"/>
		:
			<Fragment>
				<h1 className="text-center my-3">Orders</h1>
				{orders}
			</Fragment>
	)
}
