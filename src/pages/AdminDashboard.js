import React from 'react';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import { useContext, useState, useEffect } from "react";
// import { useContext, useState } from "react";
import { Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminDashboard(){
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<Tr>
						<Td>{product._id}</Td>
						<Td>{product.name}</Td>
						<Td>{product.description}</Td>
						<Td className="text-center">{product.price}</Td>
						<Td className="text-center">{product.reservee.length}</Td>
						<Td>{product.isActive ? "Active" : "Inactive"}</Td>
						<Td>
							{
								(product.isActive)
								?	
									<Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									</>
							}
						</Td>
					</Tr>					
				)
			}))
		})
	}

	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/active/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const showReservations = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			return data;
		})
	}

	useEffect(()=>{
		fetchData();
	}, [])

	return(
		(user.isAdmin) ?
		<>
			<div className="page-container">
			<div className="mb-3 text-center" style={{ color: 'white' }}>
				<h1>Admin Dashboard</h1>				
				<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button onClick={() => showReservations (user.id)} as={Link} to="" variant="success" size="lg" className="mx-2">Show Orders</Button>
			</div>
			<Table className="tables" >
		     <Thead>
		       <Tr className="text-center">
		         <Th>Product ID</Th>
		         <Th>Product Name</Th>
		         <Th>Description</Th>
		         <Th>Price</Th>
		         <Th>Reservation</Th>
		         <Th>Status</Th>
		         <Th>Action</Th>
		       </Tr>
		     </Thead>
		     <Tbody>
		       { allProducts }
		     </Tbody>
		   </Table>
		   </div>
		</>
		:
			<Navigate to="/products" />
	)
}
