import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
// import { Container, Card, Button, Row, Col } from 'react-bootstrap';

export default function Products() {

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard  key={product._id} productProp={product} />
				)
			}))

		})
	}, [])	

	return (
		
		(user.isAdmin)
		?
			<Navigate to="/admin"/>
		:
		<div className='products'>
			<Fragment>
				<h1 className="text-center my-3" style={{ color: 'white' }}>New car to reserve?</h1>
				{products}
			</Fragment>
		</div>
	)
}

// Props
	// is a shorthand for property since components are considered as object in ReactJS.
	// Props is a way to pass data from the parent to child component.
	// it is synonymous to the function parameter.
