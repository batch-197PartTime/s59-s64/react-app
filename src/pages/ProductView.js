import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// useContext hook for global state
	const { user } = useContext(UserContext);

	const navigate = useNavigate(); //useHistory

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const reserve = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/reserve`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
				title: "Successfully Reserved!",
				icon: "success",
				text: "You have successfully reserved your product."
				})

				navigate("/products");

			} else {
				Swal.fire({
				title: "Something went wrong!",
				icon: "error",
				text: "Please try again."
				})
			}

		})
	}

	useEffect(() => {
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	return (
		<Container className="page-container">
			<Row>
			  <Col lg={{span:6, offset:3}}>
			    <Card className="productCard my-3">
			        <Card.Body>
			          <Card.Title>{name}</Card.Title>
			          <Card.Subtitle>Description:</Card.Subtitle>
			          <Card.Text>{description}</Card.Text>
			          <Card.Subtitle>Price:</Card.Subtitle>
			          <Card.Text>{price}</Card.Text>			       
			          <div className="d-grip gap-2">
			          {
			          		(user.id !== null) ?
			          			<Button className="bg-primary" onClick={() => reserve(productId)}>Reserve Now</Button>
			          			:
			          			<Button className="bg-primary" as={Link} to="/login">Log in to Reserve</Button>
			          }
			          </div>
			        </Card.Body>
			      </Card>
			  </Col>
			</Row>
		</Container>
	)
}