import React from 'react';
import { CDBFooter, CDBBtn, CDBIcon, CDBBox } from 'cdbreact';
import { useLocation } from 'react-router-dom';

export default function Footer () {
  const { pathname } = useLocation();
  if ( pathname === "/products") return null;
  if ( pathname === "/admin") return null;
  return (
    <CDBFooter className="shadow">
      <CDBBox
        display="flex"
        justifyContent="between"
        alignItems="center"
        className="mx-auto py-1 flex-wrap"
        style={{ width: '70%' }}
      >
        <CDBBox display="flex" alignItems="center">
          <a href="/" className="d-flex align-items-center p-0 text-dark">          
            <span className="ml-4 h5 mb-0 font-weight-bold">Yoby1K4n0by</span>
          </a>
        </CDBBox>
        <CDBBox>
          <small className="ml-2">&copy; Yoby1K4n0by, 2022. All rights reserved.</small>
        </CDBBox>
        <CDBBox display="flex">
          <CDBBtn flat color="dark" className="p-2">
            <CDBIcon fab icon="facebook-f" />
          </CDBBtn>
          <CDBBtn flat color="dark" className="mx-3 p-2">
            <CDBIcon fab icon="twitter" />
          </CDBBtn>
          <CDBBtn flat color="dark" className="p-2">
            <CDBIcon fab icon="instagram" />
          </CDBBtn>
        </CDBBox>
      </CDBBox>
    </CDBFooter>
  );
};