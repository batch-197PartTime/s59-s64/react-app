import { Row, Col, Card, Button } from 'react-bootstrap';


export default function Highlights() {
	return (
		
		<Row>
			<Col xs={12} md={4} className="my-1">
				<Card className="mx-1">
		      		<Card.Img variant="top" src="https://tractionlife.com/wp-content/uploads/2022/04/2021-suv-models-new-and-redesigned-696x392.jpeg" />
		      		<Card.Body>
		        		<Card.Title>New and Redesigned Cars, Trucks and SUVs</Card.Title>
		        		<Card.Text>
		          			More efficient and more durable.
		        		</Card.Text>
		        		<Button variant="primary">Go to article.</Button>
		      		</Card.Body>
		    	</Card>
	    	</Col>

	    	<Col xs={12} md={4} className="my-1">
		    	<Card className="mx-1">
		      		<Card.Img variant="top" src="https://media.autoexpress.co.uk/image/private/s--uxmi5F5X--/f_auto,t_content-image-full-desktop@1/v1632929196/autoexpress/2021/09/Worlds-best-selling-cars.jpg" />
		      		<Card.Body>
		        		<Card.Title>Best selling cars, trucks and SUVs of 2022 (so far)</Card.Title>
		        		<Card.Text>
		          			Most common car in 2022?
		        		</Card.Text>
		        		<Button variant="primary">Go to article.</Button>
		      		</Card.Body>
		    	</Card>
	    	</Col>

	    	<Col xs={12} md={4} className="my-1">
		    	<Card className="mx-1">
		      		<Card.Img variant="top" src="https://techcrunch.com/wp-content/uploads/2022/01/Snapdragon-Digital-Chassis-Render-03.png" />
		      		<Card.Body>
		        		<Card.Title>10 coolest and interesting cars from CES 2022</Card.Title>
		        		<Card.Text>
		          			What is the coolest car of 2022?
		        		</Card.Text>
		        		<Button variant="primary">Go to article.</Button>
		      		</Card.Body>
		    	</Card>
	    	</Col>
    	</Row>
    			
	)
};